#include<stdio.h>
#include<math.h>
int main()
{
      int x1,y1,x2,y2;
      float res;

      printf("/n/nEnter the co-ordinates of point1 (x1,y1) :\t");
      scanf("%d%d",&x1,&y1);

      printf("\nEnter the co-ordinates of point2 (x2,y2) :\t");
      scanf("%d%d",&x2,&y2);

      res=sqrt(pow(x1-x2,2)+pow(y1-y2,2));

      printf("\nThe distance between the points is : %f/n/n",res);

      return 0;
}
