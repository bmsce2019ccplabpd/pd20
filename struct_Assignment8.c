#include<stdio.h>

struct employee
{
	int id;
	char name[20];
	float sal;
	char des[20];
	int exp;
}emp;

int main()
{
	printf("Enter the following details of the employee -\n");
	
	printf("\nID :\t");
	scanf("%d",&emp.id);
	
	printf("\nName :\t");
	scanf("%s",emp.name);
	
	printf("\nSalary :\t");
	scanf("%f",&emp.sal);
	
	printf("\nDesignation :\t");
	scanf("%s",emp.des);
	
	printf("\nExperience :\t");
	scanf("%d",&emp.exp);
	
	printf("Employee details are -\n\nID : %d\nName : %s\nSalary : %f\nDesignation : %s\nExperience : %d\n",emp.id, emp.name,emp.sal,emp.des,emp.exp);
	
	return 0;
}
	