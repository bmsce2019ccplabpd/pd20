#include<stdio.h>

int main()
{
	int arr[5][3],highest;
	highest=0;
	
	printf("Enter marks of the students in 5 subjects :\n\n");
	printf("        \tSubject 1 Subject 2 Subject 3\n\n");
	
	for(int i=0;i<5;i++)
	{
		printf("\n\nStudent %d\t",i+1);
		
		for(int j=0;j<3;j++)
		{
			printf("   ");
			scanf("%d",&arr[i][j]);
		}
	}
	
	
	for(int i=0;i<3;i++)
	{
		printf("\nHighest marks in subject %d are - ",i+1);
	
		for(int j=0;j<5;j++)
		{
			if(highest<=arr[j][i])
			{
				highest=arr[j][i];
			}
		}
		
		printf("%d",highest);
	}						
	
	return 0;
}
